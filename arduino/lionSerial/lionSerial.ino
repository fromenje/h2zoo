#include <CmdMessenger.h>

CmdMessenger cmdMessenger = CmdMessenger(Serial);

enum {
  kLightLion1, //pin: 11
  kLightLion2, //pin: 12
  kWarningLight,//pin: 13
  kContactLion1, //pin: 9
  kContactLion2 //pin: 10
};

unsigned long previousTime = 0;

void setWarningLight() {
  bool power = cmdMessenger.readBoolArg();
  cmdMessenger.sendCmd(kWarningLight, power);
  digitalWrite(13, power);
}

void setup() {
  Serial.begin(9600);
  // 3 leds : TODO check serial
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  // 2 contacteurs
	pinMode(10, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  cmdMessenger.printLfCr();
  
  //Leds output functions
  cmdMessenger.attach(kWarningLight, setWarningLight);

}

void loop() {
	int contact1, contact2;
    
  contact1 = digitalRead(9);
  digitalWrite(11, !contact1);
  cmdMessenger.sendCmd(kContactLion1, !contact1);

  delay(200);
  
  contact2 = digitalRead(10);
  digitalWrite(12, !contact2);
  cmdMessenger.sendCmd(kContactLion2, !contact2);

  delay(200);
  
  cmdMessenger.feedinSerialData();
  
}
