#include <DHT.h>
#include <Bridge.h>
#include <Process.h>

String OPENHABIP = "192.168.42.243";
DHT dht11_small(2, DHT11);
DHT dht11_big(3, DHT11);

Process heat_small;
Process light_small;
Process heat_big;
Process light1_big;
Process light2_big;

void setup() {
  Bridge.begin();
  dht11_small.begin();
  dht11_big.begin();

  // 2 heatings
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  // 3 lights
  pinMode(6, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);

  heat_small.begin("mosquitto_sub");
  heat_small.addParameter("-h");
  heat_small.addParameter(OPENHABIP);
  heat_small.addParameter("-t");
  heat_small.addParameter("heat_small");
  heat_small.runAsynchronously();

  light_small.begin("mosquitto_sub");
  light_small.addParameter("-h");
  light_small.addParameter(OPENHABIP);
  light_small.addParameter("-t");
  light_small.addParameter("light_small");
  light_small.runAsynchronously();

  heat_big.begin("mosquitto_sub");
  heat_big.addParameter("-h");
  heat_big.addParameter(OPENHABIP);
  heat_big.addParameter("-t");
  heat_big.addParameter("heat_big");
  heat_big.runAsynchronously();

  light1_big.begin("mosquitto_sub");
  light1_big.addParameter("-h");
  light1_big.addParameter(OPENHABIP);
  light1_big.addParameter("-t");
  light1_big.addParameter("light1_big");
  light1_big.runAsynchronously();

  light2_big.begin("mosquitto_sub");
  light2_big.addParameter("-h");
  light2_big.addParameter(OPENHABIP);
  light2_big.addParameter("-t");
  light2_big.addParameter("light2_big");
  light2_big.runAsynchronously();
}

void loop() {
  Process p;
  p.runShellCommand("mosquitto_pub -h " + OPENHABIP + " -t temp_small -m " + (String)dht11_small.readTemperature());
  delay(200);
  p.runShellCommand("mosquitto_pub -h " + OPENHABIP + " -t hum_small -m " + (String)dht11_small.readHumidity());
  delay(200);
  p.runShellCommand("mosquitto_pub -h " + OPENHABIP + " -t temp_big -m " + (String)dht11_big.readTemperature());
  delay(200);
  p.runShellCommand("mosquitto_pub -h " + OPENHABIP + " -t hum_big -m " + (String)dht11_big.readHumidity());
  delay(200);
  
  if (heat_small.available() > 0) {
    char c = heat_small.read();
    if(c == '0') digitalWrite(4, LOW);
    if(c == '1') digitalWrite(4, HIGH);
    heat_small.read();
  }

  if (light_small.available() > 0) {
    char c = light_small.read();
    if(c == '0') digitalWrite(6, LOW);
    if(c == '1') digitalWrite(6, HIGH);
    light_small.read();
  }

  if (heat_big.available() > 0) {
    char c = heat_big.read();
    if(c == '0') digitalWrite(5, LOW);
    if(c == '1') digitalWrite(5, HIGH);
    heat_big.read();
  }

  if (light2_big.available() > 0) {
    char c = light2_big.read();
    if(c == '0') digitalWrite(8, LOW);
    if(c == '1') digitalWrite(8, HIGH);
    light2_big.read();
  }

  if (light1_big.available() > 0) {
    char c = light1_big.read();
    if(c == '0') digitalWrite(9, LOW);
    if(c == '1') digitalWrite(9, HIGH);
    light1_big.read();
  }
}
