#include <OneWire.h>
#include <CmdMessenger.h>
#include <Adafruit_NeoPixel.h>

#define LEDSPIN 2
#define NBLEDS 93

OneWire ds(10); // on pin 10 (a 4.7K resistor is necessary)
CmdMessenger cmdMessenger = CmdMessenger(Serial);
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NBLEDS, LEDSPIN, NEO_GRB + NEO_KHZ800);

char entry_left[] = {0, 1, 2, 3, 4, 5};
char entry_right[] = {7, 8, 9, 10, 11, 12};
char cross_road_viva[] = {13, 6, 14, 15, 24};
char viva_right[] = {16, 17, 18, 19};
char viva_left[] = {23, 22, 21, 20};
char aqua_left[] = {25, 26, 27, 28, 29, 30, 31, 32, 33, 34};
char aqua_right[] = {37, 38, 39, 40, 41, 42, 43, 44, 45, 46};
char cross_road_sandbox[] = {47, 35, 36, 50, 60, 59, 49, 48};
char lion_left[] = {51, 52, 53, 54, 55, 56, 57};
char lion_right[] = {61, 62, 63, 64, 65, 66, 67};
char cross_road_lion[] = {58, 68, 69, 70, 80, 81, 82};
char lion_viva_left[] = {71, 72, 73, 74, 75, 76, 77, 78, 79};
char lion_viva_right[] = {83, 84, 85, 86, 87, 88, 89, 90, 91};

byte addr[8];
// addr : Adresse du capteur de température

enum {
  kMeasure,
  kRead
};

enum {
  kTemp,
  kHeat,
  kLights,
  kBrightness,
  kEntry,
  kCrossRoadViva,
  kViva,
  kAqua,
  kCrossRoadSandBox,
  kLion,
  kCrossRoadLion,
  kLionViva,
  kAllPaths
};

unsigned long previousTime = 0;
int state = kMeasure;

// Fonction récupérant l'adresse du capteur de température
// Retourne true si tout va bien, ou false en cas d'erreur
bool getAddress() {
  if(!ds.search(addr)) {  // Recherche un module 1-Wire
    ds.reset_search();    // Réinitialise la recherche de module
    return false;         // Retourne une erreur
  }

  if(OneWire::crc8(addr, 7) != addr[7])  // Vérifie que l'adresse a été correctement reçue
    return false;                        // Si le message est corrompu on retourne une erreur

  if (addr[0] != 0x28) // Vérifie qu'il s'agit bien d'un DS18B20
    return false;         // Si ce n'est pas le cas on retourne une erreur
}

// Fonction récupérant la température depuis le DS18B20
// Retourne true si tout va bien, ou false en cas d'erreur
void getTemperature(float *temp) {
  ds.reset();             // On reset le bus 1-Wire
  ds.select(addr);        // On sélectionne le DS18B20

  if(state == kMeasure) {
    ds.write(0x44, 1);      // On lance une prise de mesure de température
    state = kRead;
  } else if(state == kRead) {
    byte data[9];

    ds.write(0xBE);         // On envoie une demande de lecture du scratchpad

    for (byte i = 0; i < 9; i++) // On lit le scratchpad
      data[i] = ds.read();       // Et on stock les octets reçus

    // Calcul de la température en degré Celsius
    *temp = ((data[1] << 8) | data[0]) * 0.0625;
    state = kMeasure;
  }
}

void setHeat() {
  bool power = cmdMessenger.readBoolArg();
  digitalWrite(12, power);
}

void setLights() {
  bool power = cmdMessenger.readBoolArg();
  digitalWrite(11, power);
}

void setLedsBrightness() {
  int16_t brightness = cmdMessenger.readInt16Arg();
  pixels.setBrightness(brightness);
  pixels.show();
}

void lightEntry() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 6; i++) {
    if(power) {
      pixels.setPixelColor(entry_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(entry_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(entry_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(entry_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightCrossroadViva() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 5; i++) {
    if(power) {
      pixels.setPixelColor(cross_road_viva[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(cross_road_viva[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightViva() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 4; i++) {
    if(power) {
      pixels.setPixelColor(viva_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(viva_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(viva_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(viva_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightAqua() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 10; i++) {
    if(power) {
      pixels.setPixelColor(aqua_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(aqua_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(aqua_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(aqua_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightCrossroadSandbox() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 8; i++) {
    if(power) {
      pixels.setPixelColor(cross_road_sandbox[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(cross_road_sandbox[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}


void lightLion() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 7; i++) {
    if(power) {
      pixels.setPixelColor(lion_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(lion_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(lion_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(lion_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightCrossroadLion() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 7; i++) {
    if(power) {
      pixels.setPixelColor(cross_road_lion[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(cross_road_lion[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightLionViva() {
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  for(int i = 0; i < 9; i++) {
    if(power) {
      pixels.setPixelColor(lion_viva_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(lion_viva_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(lion_viva_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(lion_viva_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
  }
}

void lightAllPaths(){
  bool power = cmdMessenger.readBoolArg();
  int16_t red = cmdMessenger.readInt16Arg();
  int16_t green = cmdMessenger.readInt16Arg();
  int16_t blue = cmdMessenger.readInt16Arg();
  int delay_val = 100;
  for(int i = 0; i < 6; i++) {
    if(power) {
      pixels.setPixelColor(entry_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(entry_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(entry_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(entry_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 0; i < 5; i++) {
    if(power) {
      pixels.setPixelColor(cross_road_viva[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(cross_road_viva[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 0; i < 10; i++) {
    if(power) {
      pixels.setPixelColor(aqua_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(aqua_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(aqua_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(aqua_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 0; i < 8; i++) {
    if(power) {
      pixels.setPixelColor(cross_road_sandbox[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(cross_road_sandbox[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 0; i < 7; i++) {
    if(power) {
      pixels.setPixelColor(lion_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(lion_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(lion_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(lion_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 0; i < 7; i++) {
    if(power) {
      pixels.setPixelColor(cross_road_lion[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(cross_road_lion[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 0; i < 9; i++) {
    if(power) {
      pixels.setPixelColor(lion_viva_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(lion_viva_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(lion_viva_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(lion_viva_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }
  for(int i = 4; i > 0; i--) {
    if(power) {
      pixels.setPixelColor(viva_left[i], pixels.Color(red, green, blue));
      pixels.setPixelColor(viva_right[i], pixels.Color(red, green, blue));
    } else {
      pixels.setPixelColor(viva_left[i], pixels.Color(0, 0, 0));
      pixels.setPixelColor(viva_right[i], pixels.Color(0, 0, 0));
    }
    pixels.show();
    delay(delay_val);
  }  
}

void setup() {
  Serial.begin(9600);
  pixels.begin();
  pixels.setBrightness(30);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  while(!getAddress()) {}
  cmdMessenger.printLfCr();
  cmdMessenger.attach(kHeat, setHeat);
  cmdMessenger.attach(kLights, setLights);
  cmdMessenger.attach(kBrightness, setLedsBrightness);
  cmdMessenger.attach(kEntry, lightEntry);
  cmdMessenger.attach(kCrossRoadViva, lightCrossroadViva);
  cmdMessenger.attach(kViva, lightViva);
  cmdMessenger.attach(kAqua, lightAqua);
  cmdMessenger.attach(kCrossRoadSandBox, lightCrossroadSandbox);
  cmdMessenger.attach(kLion, lightLion);
  cmdMessenger.attach(kCrossRoadLion, lightCrossroadLion);
  cmdMessenger.attach(kLionViva, lightLionViva);
  cmdMessenger.attach(kAllPaths, lightAllPaths);
}

void loop() {
  float temp;
  unsigned long currentTime = millis();
  // Lit la température ambiante
  if(currentTime - previousTime >= 800) {
    previousTime = currentTime;
    getTemperature(&temp);
    if(state == kMeasure)
      cmdMessenger.sendCmd(kTemp, temp);
  }
  cmdMessenger.feedinSerialData();
}
