===== FICHIERS DE CONFIG POUR OPENHAB =====

Etapes pour installer et essayer de faire mumuse avec tout ça :

- Installer openhab runtime
- Installer les addons de Openhab (à dézipper dans le dossier addons)
- Aller dans le dossier openhab/configurations
- Supprimer tous les fichiers
- Cloner ce dépot Git à cet endroit
- Revenir en arriere dans le dossier principal Openhab
- Lancer start.sh en root (sudo)
- Dans votre navigateur préféré, ouvrir http://<IP_de_la_machine>:8080/openhab.app?sitemap=h2zoo

== Utilisation de Openhab Designer ==

- Si le lancement de Openhab Designer plante, installer la VM Java de Oracle
- Ouvrir un dossier > Le dossier du Git, à savoir le dossier configuration
- Go on

NB : Tous les changements aux fichiers de config (scripts, sitemap...) ne necessitent pas de redémarrer le script


Troubleshooting : Si Openhab Designer plante au lancement, voici le fix du siècle :
Ajouter la ligne suivante :
org.eclipse.swt.browser.DefaultType=mozilla
au fichier configuration/config.ini

Enjoyez bien les amis.
Bisou
